package com.demo.test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.DispatcherServlet;

public class MyCustomDispatcherServlet extends DispatcherServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2888708995934789438L;

	@Override
	protected void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		super.doService(request, response);
		
	}
	
	@Override
	protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		super.doDispatch(request, response);
	}
	
}
